---
layout: post
title: “New Blog, Who Dis?”
---

Here is the first post of a new blog that no one will read. Just have to get something up, right?

The blog uses [Jekyll](https://jekyllrb.com) and runs on [Gitlab](https://gitlab.com). The entire setup was done using an iPad. I use [Working Copy](https://workingcopyapp.com/) and [iA Writer](https://ia.net/writer) to write and post articles. At some point, I'll write up my workflow, but a quick Google will reveal many others who have already written in-depth about similar setups.

I plan to write concise posts about my musings as a public school district IT Director.

Thanks for maybe reading!